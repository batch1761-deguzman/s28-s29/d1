/* [OBJECTIVE] Create a server-side app using 
Express webb Framework*/

/*Relate this task to something that you do on a 
daily basis*/


// [SECTION]append the entire app to our node.package.manager
	/*package.json -> the "heart" of every node project.
	this alco contains different metadata that describes the 
	structure of the project*/

	/*scripts -> is used to declare and describe custom
	commands and keywords that can be used to execute this
	project with the correct runtime emvironment.*/

	/*Note: "start" is golbally recognized amonst node projects
	and frameworks as the 'default' command cript to execute a 
	task/project*/
	/*However for "unconventional" keywords or command, you have
	to append the command "run"*/
	// SYNTAX: npm run <custom command>

/* "npm install nodemon express" or "npm i nodemon express" (shortcut) install 
dependencies*/

/*Initialize a Node Package Manager
  SCRIPT:  npm init or npm init -y*/
// npm init -y (to add package.json)

// 1. identify and prepare the ingredients.
const express = require("express");

// express => weill be used as the main component to create the server
/* We need to be able to gather/aquire the utilities and components 
needed that the express library will provide us.*/
	/* => require() -> directive used to get the library/component
	  needed inside the module*/

	/* prepare the environment in which the project will be
	served*/
// [SECTION] Preparing a remote repository for our Node project
	/*NOTE: always DISABLE the no_modules folder
	  WHY? => it will take up too much space in our repository
	          making a lot more dificult to stage upon comiting the changes 
	          in our remote repository

	  		  if ever that you will deploy your node project on deployment 
	  		  platforms (heroku,netlify, vercel) the project will automatically
	  		  be rejected because node_modules is not recognized on various 
	  		  deployment platforms

	  HOW? => using a .gitignore module
	  			touch.gitignore (hidden file)*/

/*[SECTION] Create a runtime environment that
automatically autofix all the changes in out app.*/

// were going to use a utility called nodemon.
// command to install: npm install nodemon 
// another install method: npm install -g nodemon
// to initiate nodemon: nodemon fileName.index

	/*Upon starting the entry point module with nodemon, 
	you will be able ro the 'append' the application with
	yhe proper run time environment. allowing you to save
	time and effort upon commiting changes to your app*/

/*you can even send art into your run time environment  */
console.log(`
Welcome to out EXPRESS API Server!
____________________██████
_________▓▓▓▓____█████████
__ Ƹ̵̡Ӝ̵̨̄Ʒ▓▓▓▓▓=▓____▓=▓▓▓▓▓
__ ▓▓▓_▓▓▓▓░●____●░░▓▓▓▓
_▓▓▓▓_▓▓▓▓▓░░__░░░░▓▓▓▓
_ ▓▓▓▓_▓▓▓▓░░♥__♥░░░▓▓▓
__ ▓▓▓___▓▓░░_____░░░▓▓
▓▓▓▓▓____▓░░_____░░▓
_ ▓▓____ ▒▓▒▓▒___ ████
_______ ▒▓▒▓▒▓▒_ ██████
_______▒▓▒▓▒▓▒ ████████
_____ ▒▓▒▓▒▓▒_██████ ███
_ ___▒▓▒▓▒▓▒__██████ _███
_▓▓X▓▓▓▓▓▓▓__██████_ ███
▓▓_██████▓▓__██████_ ███
▓_███████▓▓__██████_ ███
_████████▓▓__██████ _███
_████████▓▓__▓▓▓▓▓▓_▒▒
_████████▓▓__▓▓▓▓▓▓
_████████▓▓__▓▓▓▓▓▓
__████████▓___▓▓▓▓▓▓
_______▒▒▒▒▒____▓▓▓▓▓▓
_______▒▒▒▒▒ _____▓▓▓▓▓
_______▒▒▒▒▒_____ ▓▓▓▓▓
_______▒▒▒▒▒ _____▓▓▓▓▓
________▒▒▒▒______▓▓▓▓▓
________█████____█████
_'▀█║────────────▄▄───────────​─▄──▄_
──█║───────▄─▄─█▄▄█║──────▄▄──​█║─█║
──█║───▄▄──█║█║█║─▄║▄──▄║█║─█║​█║▄█║
──█║──█║─█║█║█║─▀▀──█║─█║█║─█║​─▀─▀
──█║▄║█║─█║─▀───────█║▄█║─▀▀
──▀▀▀──▀▀────────────▀─█║
───────▄▄─▄▄▀▀▄▀▀▄──▀▄▄▀
──────███████───▄▀
──────▀█████▀▀▄▀
────────▀█▀

	`)